# Amdatu objectmapper

Java object to object mapping based on [Orika](http://orika-mapper.github.io/orika-docs/)

## Components
 - `org.amdatu.objectmapper.orika` -

## Dependencies
 - `javassist`


## Example usage

``` java

class Example {

  /**
   * Injected by Felix Dependency Manager
   */
  private volatile MapperFactoryService mapperFactoryService;

  public PersonDto convertToDto(PersonEntity entity){

    /**
     * Obtain a MapperFactory instance
     */
    MapperFactory mapperFactory = mapperFactoryService.createMapperFactory();

    /**
     * Define how the object should be mapped
     */
    mapperFactory.classMap(PersonEntity.class, PersonDto.class)
      .field("name", "lastName") /* name maps to lastName */
      .byDefault() /* apply default mapping for properties with the same name in both types */
      .register();

    /**
     * Obtain a MapperFacade that can be used to perform the actual mapping
     */
    MapperFacace mapperFacade = mapperFactory.getMapperFacade();

    return mapperFacade.map(entity, PersonDto.class);

  }
}

```

For more details about mapping configuration see the [Orika documentation](http://orika-mapper.github.io/orika-docs/mappings-via-classmapbuilder.html)


