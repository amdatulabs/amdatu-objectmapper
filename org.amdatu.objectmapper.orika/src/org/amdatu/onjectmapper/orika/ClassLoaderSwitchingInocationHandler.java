package org.amdatu.onjectmapper.orika;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ClassLoaderSwitchingInocationHandler implements  InvocationHandler{

	private ClassLoader classLoader;
	
	private Object delegate; 
	
	public ClassLoaderSwitchingInocationHandler(ClassLoader classLoader, Object delegate) {
		this.classLoader = classLoader;
		this.delegate = delegate;
	}
	
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		if (Object.class == method.getDeclaringClass()) {
			String name = method.getName();
			if ("equals".equals(name)) {
				return proxy == args[0];
			} else if ("hashCode".equals(name)) {
				return System.identityHashCode(proxy);
			} else if ("toString".equals(name)) {
				return proxy.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(proxy))
						+ ", with InvocationHandler " + this;
			} else {
				throw new IllegalStateException(String.valueOf(method));
			}
		}

		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			return method.invoke(delegate, args);
		}catch (InvocationTargetException e){
			throw e.getTargetException();
		}finally{
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
	}
}
