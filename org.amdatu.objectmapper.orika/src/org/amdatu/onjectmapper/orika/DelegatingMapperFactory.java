package org.amdatu.onjectmapper.orika;

import java.lang.reflect.Proxy;
import java.util.Set;

import ma.glasnost.orika.BoundMapperFacade;
import ma.glasnost.orika.DefaultFieldMapper;
import ma.glasnost.orika.Mapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.MappingHint;
import ma.glasnost.orika.ObjectFactory;
import ma.glasnost.orika.converter.ConverterFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import ma.glasnost.orika.metadata.ClassMap;
import ma.glasnost.orika.metadata.ClassMapBuilder;
import ma.glasnost.orika.metadata.MapperKey;
import ma.glasnost.orika.metadata.Type;
import ma.glasnost.orika.unenhance.UnenhanceStrategy;

@SuppressWarnings("deprecation")
public class DelegatingMapperFactory implements MapperFactory {

	private final DefaultMapperFactory delegate;
	private final ClassLoader classLoader;

	public DelegatingMapperFactory(ClassLoader classLoader) {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			this.delegate = new OrikaMapperFactory.Builder().fabulandMapperFactory(this).build();
		} finally {
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
		this.classLoader = classLoader;		
		
	}

	public void build() {
		delegate.build();
	}
	

	public <A, B> ClassMapBuilder<A, B> classMap(Class<A> arg0, Class<B> arg1) {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			return delegate.classMap(arg0, arg1);
		} finally {
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
	}

	public <A, B> ClassMapBuilder<A, B> classMap(Class<A> arg0, Type<B> arg1) {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			return delegate.classMap(arg0, arg1);
		} finally {
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
	}

	public <A, B> ClassMapBuilder<A, B> classMap(Type<A> arg0, Class<B> arg1) {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			return delegate.classMap(arg0, arg1);
		} finally {
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
	}

	public <A, B> ClassMapBuilder<A, B> classMap(Type<A> arg0, Type<B> arg1) {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			return delegate.classMap(arg0, arg1);
		} finally {
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
	}

	public boolean existsRegisteredMapper(Type<?> arg0, Type<?> arg1, boolean arg2) {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			return delegate.existsRegisteredMapper(arg0, arg1, arg2);
		} finally {
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
	}

	@SuppressWarnings("unchecked")
	public <A, B> ClassMap<A, B> getClassMap(MapperKey arg0) {
		
		ClassMap<A, B> newProxyInstance = (ClassMap<A, B>) Proxy.newProxyInstance(classLoader, new Class<?>[] { ClassMap.class },
				new ClassLoaderSwitchingInocationHandler(classLoader, delegate.getClassMap(arg0)));
		
		
		return newProxyInstance;

	}

	public ConverterFactory getConverterFactory() {
		return (ConverterFactory) Proxy.newProxyInstance(classLoader, new Class<?>[] { ConverterFactory.class },
				new ClassLoaderSwitchingInocationHandler(classLoader, delegate.getConverterFactory()));
	}

	public MapperFacade getMapperFacade() {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			return (MapperFacade) Proxy.newProxyInstance(classLoader, new Class<?>[] { MapperFacade.class },
					new ClassLoaderSwitchingInocationHandler(classLoader, delegate.getMapperFacade()));
		} finally {
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
	}

	@SuppressWarnings("unchecked")
	public <A, B> BoundMapperFacade<A, B> getMapperFacade(Class<A> arg0, Class<B> arg1, boolean arg2) {
		return (BoundMapperFacade<A, B>) Proxy.newProxyInstance(classLoader,
				new Class<?>[] { BoundMapperFacade.class }, new ClassLoaderSwitchingInocationHandler(classLoader,
						delegate.getMapperFacade(arg0, arg1, arg2)));
	}

	@SuppressWarnings("unchecked")
	public <A, B> BoundMapperFacade<A, B> getMapperFacade(Class<A> arg0, Class<B> arg1) {
		return (BoundMapperFacade<A, B>) Proxy.newProxyInstance(classLoader,
				new Class<?>[] { BoundMapperFacade.class }, new ClassLoaderSwitchingInocationHandler(classLoader,
						delegate.getMapperFacade(arg0, arg1)));
	}

	@SuppressWarnings("unchecked")
	public <A, B> BoundMapperFacade<A, B> getMapperFacade(Type<A> arg0, Type<B> arg1, boolean arg2) {
		return (BoundMapperFacade<A, B>) Proxy.newProxyInstance(classLoader,
				new Class<?>[] { BoundMapperFacade.class }, new ClassLoaderSwitchingInocationHandler(classLoader,
						delegate.getMapperFacade(arg0, arg1, arg2)));
	}

	@SuppressWarnings("unchecked")
	public <A, B> BoundMapperFacade<A, B> getMapperFacade(Type<A> arg0, Type<B> arg1) {
		return (BoundMapperFacade<A, B>) Proxy.newProxyInstance(classLoader,
				new Class<?>[] { BoundMapperFacade.class }, new ClassLoaderSwitchingInocationHandler(classLoader,
						delegate.getMapperFacade(arg0, arg1)));
	}

	public <S, D> Type<? extends D> lookupConcreteDestinationType(Type<S> arg0, Type<D> arg1, MappingContext arg2) {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			return delegate.lookupConcreteDestinationType(arg0, arg1, arg2);
		} finally {
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
	}

	public Set<Type<? extends Object>> lookupMappedClasses(Type<?> arg0) {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			return delegate.lookupMappedClasses(arg0);
		} finally {
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
	}

	public <A, B> Mapper<A, B> lookupMapper(MapperKey arg0) {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			return delegate.lookupMapper(arg0);
		} finally {
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
	}

	public <T> ObjectFactory<T> lookupObjectFactory(Type<T> arg0) {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			return delegate.lookupObjectFactory(arg0);
		} finally {
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
	}

	public Set<ClassMap<Object, Object>> lookupUsedClassMap(MapperKey arg0) {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			return delegate.lookupUsedClassMap(arg0);
		} finally {
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
	}

	public <A, B> void registerClassMap(ClassMap<A, B> arg0) {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			delegate.registerClassMap(arg0);
		} finally {
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
	}

	public <A, B> void registerClassMap(ClassMapBuilder<A, B> arg0) {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			delegate.registerClassMap(arg0);
		} finally {
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
	}

	public void registerConcreteType(Class<?> arg0, Class<?> arg1) {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			delegate.registerConcreteType(arg0, arg1);
		} finally {
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
	}

	public void registerConcreteType(Type<?> arg0, Type<?> arg1) {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			delegate.registerConcreteType(arg0, arg1);
		} finally {
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
	}

	public void registerDefaultFieldMapper(DefaultFieldMapper... arg0) {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			delegate.registerDefaultFieldMapper(arg0);
		} finally {
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
	}

	public <A, B> void registerMapper(Mapper<A, B> arg0) {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			delegate.registerMapper(arg0);
		} finally {
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
	}

	public void registerMappingHint(MappingHint... arg0) {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			delegate.registerMappingHint(arg0);
		} finally {
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
	}

	public <T> void registerObjectFactory(ObjectFactory<T> arg0, Class<T> arg1) {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			delegate.registerObjectFactory(arg0, arg1);
		} finally {
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
	}

	public <T> void registerObjectFactory(ObjectFactory<T> arg0, Type<T> arg1) {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			delegate.registerObjectFactory(arg0, arg1);
		} finally {
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
	}

	@Override
	public UnenhanceStrategy getUserUnenhanceStrategy() {
		return (UnenhanceStrategy) Proxy.newProxyInstance(classLoader,
				new Class<?>[] { UnenhanceStrategy.class }, new ClassLoaderSwitchingInocationHandler(classLoader,
						delegate.getUserUnenhanceStrategy()));
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T, S> ObjectFactory<T> lookupObjectFactory(Type<T> arg0, Type<S> arg1) {
		return ( ObjectFactory<T>) Proxy.newProxyInstance(classLoader,
				new Class<?>[] { ObjectFactory.class }, new ClassLoaderSwitchingInocationHandler(classLoader,
						delegate.lookupObjectFactory(arg0, arg1)));
		
	}


	@Override
	public <T, S> void registerObjectFactory(ObjectFactory<T> arg0, Type<T> arg1, Type<S> arg2) {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(classLoader);
			delegate.registerObjectFactory(arg0, arg1, arg2);
		} finally {
			Thread.currentThread().setContextClassLoader(contextClassLoader);
		}
	}

}
