package org.amdatu.onjectmapper.orika;

import org.amdatu.objectmapper.MapperFactoryService;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1)
			throws Exception {

	}

	@Override
	public void init(BundleContext arg0, DependencyManager dm) throws Exception {

		dm.add(createComponent().setInterface(MapperFactoryService.class.getName(),
				null).setImplementation(MapperFactoryServiceFactory.class));

	}

}
