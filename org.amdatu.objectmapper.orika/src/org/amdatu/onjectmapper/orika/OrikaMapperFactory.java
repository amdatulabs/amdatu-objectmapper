package org.amdatu.onjectmapper.orika;

import ma.glasnost.orika.impl.DefaultMapperFactory;
import ma.glasnost.orika.metadata.ClassMapBuilderFactory;

class OrikaMapperFactory extends DefaultMapperFactory {

	private final DelegatingMapperFactory fabulandMapperFactory;

	protected OrikaMapperFactory(DelegatingMapperFactory fabulandMapperFactory, MapperFactoryBuilder<?, ?> arg0) {
		super(arg0);
		this.fabulandMapperFactory = fabulandMapperFactory;
	}
	
    public static class Builder extends MapperFactoryBuilder<OrikaMapperFactory, Builder> {
        
    	private DelegatingMapperFactory fabulandMapperFactory;
    	

        @Override
        public OrikaMapperFactory build() {
            return new OrikaMapperFactory(fabulandMapperFactory, this);
        }
        
        public Builder fabulandMapperFactory(DelegatingMapperFactory fabulandMapperFactory) {
			this.fabulandMapperFactory = fabulandMapperFactory;			
			
			return self();
		}
        
        @Override
        protected Builder self() {
            return this;
        }
        
    }
	
	@Override
	protected ClassMapBuilderFactory getClassMapBuilderFactory() {
		ClassMapBuilderFactory test = super.getClassMapBuilderFactory();
		test.setMapperFactory(this.fabulandMapperFactory);
		return test;
	}
	
	
	@Override
	protected void addClassMapBuilderFactory(ClassMapBuilderFactory factory) {
		super.addClassMapBuilderFactory(factory);
		factory.setMapperFactory(this.fabulandMapperFactory);
	}
}