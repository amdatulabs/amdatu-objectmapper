package org.amdatu.onjectmapper.orika;

import ma.glasnost.orika.MapperFactory;

import org.amdatu.objectmapper.MapperFactoryService;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceFactory;
import org.osgi.framework.ServiceRegistration;
import org.osgi.framework.wiring.BundleWiring;

public class MapperFactoryServiceFactory implements ServiceFactory<MapperFactoryService> {

	@Override
	public MapperFactoryService getService(Bundle bundle, ServiceRegistration<MapperFactoryService> registration) {
		final BundleWiring bundleWiring = (BundleWiring)bundle.adapt(BundleWiring.class);

		final ClassLoader mapperClassLoader = getClass().getClassLoader();
		
		return new MapperFactoryService() {

			@Override
			public MapperFactory createMapperFactory() {
				ObjectaMapperClassLoader mappingClassLoader = new ObjectaMapperClassLoader(mapperClassLoader,
						bundleWiring.getClassLoader());
				return new DelegatingMapperFactory(mappingClassLoader);
			}
		};
	}

	@Override
	public void ungetService(Bundle bundle, ServiceRegistration<MapperFactoryService> registration,
			MapperFactoryService service) {
	}

	class ObjectaMapperClassLoader extends ClassLoader {

		private ClassLoader cl;

		public ObjectaMapperClassLoader(ClassLoader parent, ClassLoader bundle) {
			super(parent);
			this.cl = bundle;
		}

		@Override
		public Class<?> loadClass(String name) throws ClassNotFoundException {
			try {
				return cl.loadClass(name);
			} catch (ClassNotFoundException | NoClassDefFoundError e) {
				return super.loadClass(name);
			}
		}

	}

}
