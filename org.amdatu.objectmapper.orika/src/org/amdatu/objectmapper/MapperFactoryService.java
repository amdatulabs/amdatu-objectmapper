package org.amdatu.objectmapper;

import ma.glasnost.orika.MapperFactory;

public interface MapperFactoryService {

	MapperFactory createMapperFactory();
	
}
