package org.amdatu.objectmapper.orika.test;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;

import org.amdatu.bndtools.test.BaseOSGiServiceTest;
import org.amdatu.objectmapper.MapperFactoryService;

public class MapperFactoryServiceTest extends BaseOSGiServiceTest<MapperFactoryService> {

	public MapperFactoryServiceTest() {
		super(MapperFactoryService.class);
	}
	
	public void testMapperFactoryServiceAvailable(){
		assertNotNull(instance);
	}

	public void testDtoToEntity(){
		MapperFactory mapperFactory = instance.createMapperFactory();
		MapperFacade mapperFacade = mapperFactory.getMapperFacade();
		
		MyDto dto = new MyDto();
		dto.setId("test-id");
		dto.setName("test-name");

		MyEntity entity = mapperFacade.map(dto, MyEntity.class);
		assertNotNull(entity);
		assertEquals(dto.getId(), entity.getId());
		assertEquals(dto.getName(), entity.getName());
		
	}
	
	public void testEntityToDto(){
		MapperFactory mapperFactory = instance.createMapperFactory();
		MapperFacade mapperFacade = mapperFactory.getMapperFacade();

		
		MyEntity entity = new MyEntity();
		entity.setId("test-id");
		entity.setName("test-name");
	
		
		MyDto dto = mapperFacade.map(entity, MyDto.class);
		assertNotNull(entity);
		assertEquals(entity.getId(), dto.getId());
		assertEquals(entity.getName(), dto.getName());
		
	}
	
}
