package org.amdatu.objectmapper.orika.test;

import java.util.List;

public class MyDto {

	private String id; 
	
	private String name;
	
	private List<MyRelatedDto> related;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<MyRelatedDto> getRelated() {
		return related;
	}

	public void setRelated(List<MyRelatedDto> related) {
		this.related = related;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MyDto other = (MyDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MyDto [id=" + id + ", name=" + name + "]";
	}

	
}
